//
//  OIMWoomElem.h
//  Pods
//
//  Created by Mouayed on 26/03/2023.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OIMWoomElem : NSObject


@property (nonatomic, nullable, copy) NSString *fileId;
@property (nonatomic, nullable, copy) NSString *coverUrl;
@property (nonatomic, nullable, copy) NSString *mediaUrl;
@property (nonatomic, nullable, copy) NSString *userId;
@property (nonatomic, nullable, copy) NSString *nickname;
@property (nonatomic, nullable, copy) NSString *faceUrl;

@end

NS_ASSUME_NONNULL_END
