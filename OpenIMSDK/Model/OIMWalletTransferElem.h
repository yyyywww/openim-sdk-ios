//
//  OIMWalletTransferElem.h
//  Pods
//
//  Created by Mouayed on 06/03/2023.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface OIMWalletTransferElem : NSObject


@property (nonatomic, nullable, copy) NSString *transactionID;
@property (nonatomic, nullable, copy) NSString *senderID;
@property (nonatomic, nullable, copy) NSString *amount;
@property (nonatomic, nullable, copy) NSString *currency;

@end

NS_ASSUME_NONNULL_END
