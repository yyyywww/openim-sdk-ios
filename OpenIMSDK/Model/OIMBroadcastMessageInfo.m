//
//  OIMBroadcastMessageInfo.m
//  OpenIMSDK
//
//  Created by Mouayed on 09/02/2023.
//

#import <Foundation/Foundation.h>



#import "OIMBroadcastMessageInfo.h"

@implementation OIMReceiverInfo

@end

@implementation OIMBroadcastMessageInfo

+ (NSDictionary *)mj_objectClassInArray
{
    return @{@"local_broadcast_msg_receiver_list" : [OIMReceiverInfo class]};
}

@end
