//
//  OIMBroadcastMessageInfo.h
//  Pods
//
//  Created by Mouayed on 09/02/2023.
//

#import <Foundation/Foundation.h>
#import "OIMMessageInfo.h"
#import "OIMModelDefine.h"

NS_ASSUME_NONNULL_BEGIN

/// OIMReceiverInfo
///
@interface OIMReceiverInfo : NSObject

@property (nonatomic, nullable, copy) NSString *clientMsgID;
@property (nonatomic, nullable, copy) NSString *serverMsgID;
@property (nonatomic, nullable, copy) NSString *sendID;
@property (nonatomic, nullable, copy) NSString *senderNickname;
@property (nonatomic, nullable, copy) NSString *senderFaceURL;
@property (nonatomic, assign) OIMMessageStatus status;
@property (nonatomic, assign) NSInteger receiverType;
@property (nonatomic, assign) NSTimeInterval createTime;

@end

/// OIMBroadcastMessageInfo
///
@interface OIMBroadcastMessageInfo : NSObject

@property (nonatomic, nullable, copy) NSArray<OIMReceiverInfo *> *local_broadcast_msg_receiver_list;

@property (nonatomic, nullable, strong) OIMMessageInfo *local_broadcast_chat_log;

@property (nonatomic, assign) NSInteger total_receiver_count;

@end



NS_ASSUME_NONNULL_END
