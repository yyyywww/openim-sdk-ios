//
//  OIMBroadcastMessageInfo.h
//  Pods
//
//  Created by Mouayed on 09/02/2023.
//

#import <Foundation/Foundation.h>
#import "OIMMessageInfo.h"
#import "OIMModelDefine.h"

NS_ASSUME_NONNULL_BEGIN

/// OIMReceiverInfo
///
@interface OIMReceiverInfo : NSObject

@property (nonatomic, nullable, copy) NSString *recvID;
@property (nonatomic, nullable, copy) NSString *recvName;
@property (nonatomic, assign) NSInteger msgType;

@end

/// OIMBroadcastMessageInfo
///
@interface OIMBroadcastMessageInfo : NSObject

@property (nonatomic, assign) NSInteger id;

@property (nonatomic, assign) OIMMessageContentType contentType;

@property (nonatomic, nullable, copy) NSString *sender_id;

@property (nonatomic, nullable, copy) NSString *create_time;

@property (nonatomic, nullable, copy) NSArray<OIMReceiverInfo *> *receiver;

@property (nonatomic, nullable, strong) OIMMessageInfo *message;


@end



NS_ASSUME_NONNULL_END
